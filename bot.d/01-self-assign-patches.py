"""
Self-assigns unassigned patches in the `New` state to the particular maintainer and
changes state to `Under review`.
"""

import re
import os
import sys
import logging

from dataclasses import dataclass

from git_pw import api
from git_pw import config

CONF = config.CONF
CONF.debug = os.getenv("PW_DEBUG")
CONF.token = os.getenv("PW_TOKEN")
CONF.server = os.getenv("PW_SERVER")
CONF.project = os.getenv("PW_PROJECT")


@dataclass
class MaintainerItem:
    user_id: int
    username: str
    emails: list


class Maintainers:
    def __init__(self, api, project):
        self.api = api
        self.project = project
        self.m_email = {}
        self.m_username = {}
        self.init()

    def init(self):
        self.init_from_patchwork()
        self.init_from_mailmap()

    def from_username(self, username):
        return self.m_username.get(username)

    def from_email(self, email):
        return self.m_email.get(email)

    def update_user_id(self, maintainer):
        """ Fixes wrong user_id returned by project maintainers list """
        m = api.index("users", [("q", maintainer.username)])
        assert len(m) == 1
        maintainer = self.from_username(maintainer.username)
        maintainer.user_id = m[0].get("id")
        return maintainer

    def init_from_patchwork(self):
        r = self.api.index(f"projects/{self.project}")
        if not r:
            return

        for m in r.get("maintainers"):
            user_id = 0  # bogus, set to 0, fetch real later
            email = m["email"]
            username = m["username"]

            logging.debug(f"patchwork: adding {email} to {username}")

            n = MaintainerItem(user_id, username, [email])
            self.m_email[email] = n
            self.m_username[username] = n

    def init_from_mailmap(self, path=".mailmap"):
        with open(path, "r") as fd:
            for line in fd:
                username, email = line.rstrip().split()
                logging.debug(f".mailmap: adding {email} to {username}")

                m = self.from_username(username)
                if not m:
                    logging.error("No such username: %s", username)
                    sys.exit(1)

                m.emails.append(email)
                self.m_email[email] = m
                self.m_username[username] = m


def get_patches(url, params=None):
    r = []
    rsp = api._get(url, params)
    patches = rsp.json()
    for patch in patches:
        r.append(patch)

    links = rsp.headers.get("Link")
    if not links:
        return r

    patches = new_patches_next_page(links)
    for patch in patches:
        r.append(patch)

    return r


def new_patches_next_page(links):
    url = None

    re_link = re.compile(r'<(.*)>; rel="(.*)"')
    for link in links.split(","):
        match = re_link.search(link)
        rel = match.group(2)
        if rel == "next":
            url = match.group(1)
            break

    if not url:
        return []

    return get_patches(url)


def new_patches():
    url = "/".join([CONF.server, "patches", ""])
    params = [("project", CONF.project), ("state", "new"), ("order", "date")]

    return get_patches(url, params)


def patch_assign_maintainer(patch_id, delegate_id):
    api.update(
        "patches", patch_id, [("state", "Under Review"), ("delegate", delegate_id)]
    )


def process_patch(patch, maintainers):
    patch_id = patch["id"]
    subject = patch["name"]
    delegate = patch["delegate"]
    email = patch["submitter"].get("email")

    if not email:
        logging.warning(f"{subject}/{patch_id} has missing email")
        return

    if delegate:
        delegate = maintainers.from_email(delegate.get("email"))
        logging.debug(f"{subject} ({email}) is already assigned to {delegate.username}")
        return

    maintainer = maintainers.from_email(email)
    if not maintainer:
        logging.debug(f"{subject} by {email} is not from maintainer")
        return

    logging.info(
        f"Marking '{subject}'({patch_id}) as 'Under Review' by {maintainer.username}"
    )

    if maintainer.user_id == 0:
        maintainer = maintainers.update_user_id(maintainer)

    patch_assign_maintainer(patch_id, maintainer.user_id)


def main():
    logging.basicConfig(
        level=logging.INFO, format="%(levelname)7s: %(message)s", stream=sys.stderr
    )

    if CONF.debug:
        logging.getLogger().setLevel(logging.DEBUG)

    maintainers = Maintainers(api, CONF.project)
    for patch in new_patches():
        process_patch(patch, maintainers)


main()
