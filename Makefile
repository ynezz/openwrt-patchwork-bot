-include local.mk

.PHONY: lint run
VENV := venv

all: lint run

$(VENV)/.base: requirements/base.txt
	python3 -mvenv $(VENV)
	pip install -r requirements/base.txt
	touch $@

$(VENV)/.development: $(VENV)/.base requirements/development.txt
	pip install -r requirements/development.txt
	touch $@

lint: $(VENV)/.development
	black bot.d
	flake8 bot.d

run: $(VENV)/.base
	$(VENV)/bin/python3 bot.d/*.py
